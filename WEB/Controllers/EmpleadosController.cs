﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MODELO;

namespace WEB.Controllers
{
    public class EmpleadosController : ApiController
    {
        NorthwindEntities ne = new NorthwindEntities();

        [HttpGet]
        public IEnumerable<SP_LISTAR_EMPLEADOS_Result> ListaEmpleados()
        {
            var lista = ne.SP_LISTAR_EMPLEADOS().ToList();
            return lista;
        }
    }
}
